<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>
<style>
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", Arial, Helvetica, sans-serif
    }
    
    .myLink {
        display: none
    }
    
    .fa-anchor,
    .fa-coffee {
        font-size: 200px
    }

</style>

<body>
    <div>
        <div class="w3-container w3-red">
            <h1>Summer Holiday</h1>
        </div>

        <div class="w3-row-padding w3-margin-top">

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_5terre.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>5 Terre</h4>
                    </div>
                </div>
            </div>

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_monterosso.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>Monterosso</h4>
                    </div>
                </div>
            </div>

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_vernazza.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>Vernazza</h4>
                    </div>
                </div>
            </div>

        </div>
        <div class="w3-row-padding w3-margin-top">

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_manarola.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>Manarola</h4>
                    </div>
                </div>
            </div>

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_corniglia.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>Corniglia</h4>
                    </div>
                </div>
            </div>

            <div class="w3-third">
                <div class="w3-card">
                    <img src="img_riomaggiore.jpg" style="width:100%">
                    <div class="w3-container">
                        <h4>Riomaggiore</h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
</body>

</html>

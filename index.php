<!DOCTYPE html>
<html>
<title>Quickie</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>
<style>
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", Arial, Helvetica, sans-serif
    }
    
    .myLink {
        display: non
    }
    
    .fa-anchor,
    .fa-coffee {
        font-size: 200px
    }

</style>

<body class="w3-light-grey">


    <!-- Navbar -->
    <div class="w3-top">
        <div class="w3-bar w3-black w3-card w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
            <a href="#home" class="w3-bar-item w3-button w3-text-red"><b><i class="fa fa-map-marker w3-margin-right"></i>Q-POS</b></a>
            <a href="#home" class="w3-bar-item w3-button w3-padding-large w3-red">Home</a>
            <a href="#tour" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-red">About Quickie</a>
            <a href="#contact" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-red">Contact</a>
            <div class="w3-right">
                <a href="register.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-red">Sign Up</a>
                <a href="login.php" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-red"><i class="fa fa-unlock" style="color:red"></i> Log In</a>
            </div>
        </div>

        <!-- Navbar on small screens -->
        <div id="navDemo" class="w3-bar-block w3-black w3-hide w3-hide-large w3-hide-medium w3-large w3-text-red">
            <a href="signup.php" class="w3-bar-item w3-button w3-padding-large">Sign Up</a>
            <a href="login.php" class="w3-bar-item w3-button w3-padding-large"><i class="fa fa-unlock" style="color:red"></i>Log In</a>
            <a href="#tour" class="w3-bar-item w3-button w3-padding-large">About Quickie</a>
            <a href="#contact" class="w3-bar-item w3-button w3-padding-large">Contact</a>
        </div>
    </div>


    <!-- Navigation Bar -->
    <!--
<div class="w3-bar w3-white w3-border-bottom w3-xlarge">
    <a href="#" class="w3-bar-item w3-button w3-text-red w3-hover-red"><b><i class="fa fa-map-marker w3-margin-right"></i>Logo</b></a>
    <a href="#" class="w3-bar-item w3-button w3-right w3-hover-red w3-text-grey"><i class="fa fa-search"></i></a>
</div>
-->

    <div id="home">
        <header class="w3-container w3-red w3-center" style="padding:128px 16px">

            <h1 class="w3-margin w3-jumbo w3-text-black">Welcome to Quickie</h1>
            <p class="w3-xlarge w3-text-black">A simple POS for your Business</p>
            <a href="#tour" class="w3-bar-item w3-button w3-padding-large w3-margin-top w3-black">Get Started Now</a>

            <!--        <button class="w3-button w3-black w3-padding-large w3-large w3-margin-top">Get Started</button>-->
        </header>

    </div>
    <!-- Header -->
    <!--
    <header class="w3-display-container w3-content w3-red" style="max-width:1500px">
        <img class="w3-image" src="london2.jpg" alt="London" width="1500" height="700">
        <div class="w3-display-middle" style="width:65%">
-->


    <!--
            <div class="w3-bar w3-black">
                <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'Flight');"><i class="fa fa-plane w3-margin-right"></i>Flight</button>
                <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'Hotel');"><i class="fa fa-bed w3-margin-right"></i>Hotel</button>
                <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'Car');"><i class="fa fa-car w3-margin-right"></i>Rental</button>
            </div>
-->

    <!-- Tabs -->
    <!--
<div id="Flight" class="w3-container w3-white w3-padding-16 myLink">
    <h3>Travel the world with us</h3>
    <div class="w3-row-padding" style="margin:0 -16px;">
        <div class="w3-half">
            <label>From</label>
            <input class="w3-input w3-border" type="text" placeholder="Departing from">
        </div>
        <div class="w3-half">
            <label>To</label>
            <input class="w3-input w3-border" type="text" placeholder="Arriving at">
        </div>
    </div>
    <p><button class="w3-button w3-dark-grey">Search and find dates</button></p>
</div>

<div id="Hotel" class="w3-container w3-white w3-padding-16 myLink">
    <h3>Find the best hotels</h3>
    <p>Book a hotel with us and get the best fares and promotions.</p>
    <p>We know hotels - we know comfort.</p>
    <p><button class="w3-button w3-dark-grey">Search Hotels</button></p>
</div>

<div id="Car" class="w3-container w3-white w3-padding-16 myLink">
    <h3>Best car rental in the world!</h3>
    <p><span class="w3-tag w3-deep-orange">DISCOUNT!</span> Special offer if you book today: 25% off anywhere in the world with CarServiceRentalRUs</p>
    <input class="w3-input w3-border" type="text" placeholder="Pick-up point">
    <p><button class="w3-button w3-dark-grey">Search Availability</button></p>
</div>
-->
    <!--
        </div>
    </header>
-->

    <!-- Page content -->
    <div class="w3-content" style="max-width:1100px;">

        <!-- Good offers -->
        <div class="w3-container w3-margin-top" id="tour">
            <h2>
                <center>A Quickie Tour</center>
            </h2>
            <!--            <h6>Up to <strong>50%</strong> discount.</h6>-->
        </div>
        <!--
<div class="w3-row-padding w3-text-white w3-large">
    <div class="w3-half w3-margin-bottom">
        <div class="w3-display-container">
            <img src="cinqueterre.jpg" alt="Cinque Terre" style="width:100%">
            <span class="w3-display-bottomleft w3-padding">Cinque Terre</span>
        </div>
    </div>
    <div class="w3-half">
        <div class="w3-row-padding" style="margin:0 -16px">
            <div class="w3-half w3-margin-bottom">
                <div class="w3-display-container">
                    <img src="newyork2.jpg" alt="New York" style="width:100%">
                    <span class="w3-display-bottomleft w3-padding">New York</span>
                </div>
            </div>
            <div class="w3-half w3-margin-bottom">
                <div class="w3-display-container">
                    <img src="sanfran.jpg" alt="San Francisco" style="width:100%">
                    <span class="w3-display-bottomleft w3-padding">San Francisco</span>
                </div>
            </div>
        </div>
        <div class="w3-row-padding" style="margin:0 -16px">
            <div class="w3-half w3-margin-bottom">
                <div class="w3-display-container">
                    <img src="pisa.jpg" alt="Pisa" style="width:100%">
                    <span class="w3-display-bottomleft w3-padding">Pisa</span>
                </div>
            </div>
            <div class="w3-half w3-margin-bottom">
                <div class="w3-display-container">
                    <img src="paris.jpg" alt="Paris" style="width:100%">
                    <span class="w3-display-bottomleft w3-padding">Paris</span>
                </div>
            </div>
        </div>
    </div>
</div>
-->





        <!-- Explore Nature -->
        <div class="w3-container">
            <h3>Simplicity is Key</h3>
            <p>Travel with us and see nature at its finest.</p>
        </div>
        <div class="w3-row-padding">
            <div class="w3-half w3-margin-bottom">
                <img src="ocean2.jpg" alt="Norway" style="width:100%">
                <div class="w3-container w3-white">
                    <h3>Simplicity is Key</h3>
                    <p class="w3-opacity">Roundtrip from $79</p>
                    <p>Praesent tincidunt sed tellus ut rutrum sed vitae justo.</p>
                    <button class="w3-button w3-margin-bottom">Buy Tickets</button>
                </div>
            </div>
            <div class="w3-half w3-margin-bottom">
                <img src="mountains2.jpg" alt="Austria" style="width:100%">
                <div class="w3-container w3-white">
                    <h3>Improve your Business Decisions</h3>
                    <p class="w3-opacity">One-way from $39</p>
                    <p>Praesent tincidunt sed tellus ut rutrum sed vitae justo.</p>
                    <button class="w3-button w3-margin-bottom">Buy Tickets</button>
                </div>
            </div>
        </div>


        <!-- First Grid -->
        <div class="w3-row-padding w3-padding-64 w3-container">
            <div class="w3-content">
                <div class="w3-twothird">
                    <h1>Lorem Ipsum</h1>
                    <h5 class="w3-padding-32">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>


                </div>

                <div class="w3-third w3-center">
                    <i class="fa fa-anchor w3-padding-64 w3-text-red"></i>
                </div>
            </div>
        </div>

        <!-- Second Grid -->
        <div class="w3-row-padding w3-light-grey w3-padding-64 w3-container">
            <div class="w3-content">
                <div class="w3-third w3-center">
                    <i class="fa fa-coffee w3-padding-64 w3-text-red w3-margin-right"></i>
                </div>

                <div class="w3-twothird">
                    <h1>Lorem Ipsum</h1>
                    <h5 class="w3-padding-32">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>


                </div>
            </div>
        </div>



        <div class="w3-row-padding w3-text-white w3-large">
            <div class="w3-half w3-margin-bottom">
                <div class="w3-display-container">
                    <img src="cinqueterre.jpg" alt="Cinque Terre" style="width:100%">
                    <span class="w3-display-bottomleft w3-padding">Cinque Terre</span>
                </div>
            </div>
            <div class="w3-half">
                <div class="w3-row-padding" style="margin:0 -16px">
                    <div class="w3-half w3-margin-bottom">
                        <div class="w3-display-container">
                            <img src="newyork2.jpg" alt="New York" style="width:100%">
                            <span class="w3-display-bottomleft w3-padding">New York</span>
                        </div>
                    </div>
                    <div class="w3-half w3-margin-bottom">
                        <div class="w3-display-container">
                            <img src="sanfran.jpg" alt="San Francisco" style="width:100%">
                            <span class="w3-display-bottomleft w3-padding">San Francisco</span>
                        </div>
                    </div>
                </div>
                <div class="w3-row-padding" style="margin:0 -16px">
                    <div class="w3-half w3-margin-bottom">
                        <div class="w3-display-container">
                            <img src="pisa.jpg" alt="Pisa" style="width:100%">
                            <span class="w3-display-bottomleft w3-padding">Pisa</span>
                        </div>
                    </div>
                    <div class="w3-half w3-margin-bottom">
                        <div class="w3-display-container">
                            <img src="paris.jpg" alt="Paris" style="width:100%">
                            <span class="w3-display-bottomleft w3-padding">Paris</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div class="w3-container" id="about">
            <h2>
                <center>Features</center>
            </h2>
            <div class="w3-row-padding w3-center" style="margin-top:25px">
                <div class="w3-quarter">
                    <i class="fa fa-desktop w3-margin-bottom w3-jumbo w3-center"></i>
                    <p class="w3-large">Online Presence</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
                <div class="w3-quarter">
                    <i class="fa fa-building w3-margin-bottom w3-jumbo"></i>
                    <p class="w3-large">Organizations</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
                <div class="w3-quarter">
                    <i class="fa fa-diamond w3-margin-bottom w3-jumbo"></i>
                    <p class="w3-large">Jobs</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
                <div class="w3-quarter">
                    <i class="fa fa-cog w3-margin-bottom w3-jumbo"></i>
                    <p class="w3-large">Support</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
            </div>
        </div>

        <!-- Newsletter -->
        <div class="w3-container">
            <div class="w3-panel w3-padding-16 w3-black w3-card w3-hover-opacity-off">
                <h2>
                    <center>Get the best offers first!</center>
                </h2>
                <h4>
                    <center>Let us transform your business turnover!</center>
                </h4>
                <h5>
                    <center>Join our newsletter.</center>
                </h5>
                <!--                <label>E-mail</label>-->
                <input class="w3-input w3-border" type="email" placeholder="Your Email address">
                <center><button type="button" class="w3-button w3-red w3-margin-top">Subscribe</button></center>
            </div>
        </div>

        <!-- Contact -->
        <div class="w3-container" id="contact">


            <h2 id="contact">
                <center>Contact Us</center>
            </h2>
            <p>
                <center>Get In Touch With Us</center>
            </p>
            <i class="fa fa-map-marker" style="width:30px"></i> Nairobi, Kenya<br>
            <i class="fa fa-phone" style="width:30px"></i> Phone: +00 151515<br>
            <i class="fa fa-envelope" style="width:30px"> </i> Email: quickiepos@gmail.com<br>
            <center>
                <form action="https://www.w3schools.com/action_page.php" target="_blank">
                    <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Name" required name="Name"></p>
                    <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Email" required name="Email"></p>
                    <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Message" required name="Message"></p>
                    <p><button class="w3-button w3-black w3-padding-large" type="submit">SEND MESSAGE</button></p>
                </form>
            </center>
        </div>

        <!-- End page content -->
    </div>

    <!-- Footer -->
    <footer class="w3-container w3-center w3-black">
        <b><h3 class=" w3-text-red">Find Us On</h3></b>
        <div class="w3-xlarge w3-padding-16">
            <i class="fa fa-facebook-official w3-hover-opacity"></i>
            <i class="fa fa-instagram w3-hover-opacity"></i>
            <i class="fa fa-snapchat w3-hover-opacity"></i>
            <i class="fa fa-pinterest-p w3-hover-opacity"></i>
            <i class="fa fa-twitter w3-hover-opacity"></i>
            <i class="fa fa-linkedin w3-hover-opacity"></i>
        </div>
        <div id="top">

            <p class=" w3-text-red">Powered by <a href="index.php" target="_blank" class="w3-hover-text-red">Quickie</a></p>
            <a href="#home" class="w3-button"><i class="fa fa-arrow-circle-o-up w3-padding w3-xxxlarge w3-text-red"></i>  </a>

        </div>
    </footer>

    <script>
        // Tabs
        function openLink(evt, linkName) {
            var i, x, tablinks;
            x = document.getElementsByClassName("myLink");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < x.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
            }
            document.getElementById(linkName).style.display = "block";
            evt.currentTarget.className += " w3-red";
        }
        // Click on the first tablink on load
        document.getElementsByClassName("tablink")[0].click();

        // Used to toggle the menu on small screens when clicking on the menu button
        function myFunction() {
            var x = document.getElementById("navDemo");
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }

    </script>

</body>



</html>
